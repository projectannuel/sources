<?php
class PageController{

	public function indexAction($params){
	
		$v = new View("showPage", "back");
	}

	public function editAction($params){
		$element = new Element();			

		$allElements = $element->getById($params[0]);

		$v = new View("showPage", "back");
		$v->assign("allElements",$allElements);
	}

	public function createAction($params){

		echo "create";

		$pageName = "truc";
		$templateId = 777;

		$page = new Page();
		$page->setUserId("1");
		$page->setName($pageName);
		$page->setTemplateId($templateId);
		$page->setTypePageId(1);

		print_r($page);

		$page->save();

		$pageId = $page->getIdByName($pageName);

		$typeElement = new TypeElement();	
		$element = new Element();

		$form = $element->elementFormEdit();
		$errors = [];
		$lastValues = [];

		if(!empty($params["POST"])){
			
			$errors = Validator::validateA($form, $params["POST"]);

			if(empty($errors)){
				foreach($params["POST"] as $val){
					$elem = new Element();
					$elem->typeelement = null;
					$elem->$pagesId = $pageId;
					$elem->$value=$val;
					$elem->save();
				}

				//$theme->save();
			}
		}

		$v = new View("createPage", "back");
		$v->assign("errors",$errors);
		$v->assign("config",$form);
		$v->assign("lastValues",$lastValues);
		
	}

}