<?php
class TemplateController{

	public function indexAction($params){
        $template = new Template();
		$form = $template->configFormEdit();

		if(!empty($params["POST"])){
			
			$errors = Validator::validateA($form, $params["POST"]);

			if(empty($errors)){
				$template->setActive($params["POST"]["active"]);
				$template->save();
			}
		}
		
		$v = new View("showpage", "back");
		$v->assign("config",$form);
	}
}