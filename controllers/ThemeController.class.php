<?php
class ThemeController{

	public function indexAction($params){
        $theme = new Theme();
		$form = $theme->configFormEdit();

		if(!empty($params["POST"])){
			
			$errors = Validator::validateA($form, $params["POST"]);

			if(empty($errors)){
                
				$theme->setFont1($params["POST"]["font1"]);
				$theme->setFont2($params["POST"]["font2"]);
				$theme->setFontCOlor($params["POST"]["fontcolor"]);
				$theme->setColor1($params["POST"]["color1"]);
                $theme->setColor2($params["POST"]["color2"]);
                $theme->setColor3($params["POST"]["color3"]);
                $theme->setColor4($params["POST"]["color4"]);
                $theme->setColorBackground($params["POST"]["colorbackground"]);
				$theme->setLogo($params["POST"]["logo"]);
				$theme->setChoice($params["POST"]["choice"]);

				$theme->save();
			}
		}

		$lastValues = $theme->getLastNbElements(5);
		
		$v = new View("editTheme", "back");
		$v->assign("config",$form);
		$v->assign("errors",$errors);
		$v->assign("lastValues",$lastValues);
		
	}
}