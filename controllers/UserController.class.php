<?php

require_once("services/Mailer.php");

class UserController extends GenericController {

	//Mdp oublie
	public function forgetPwdAction($params) {
		
		$user = new User();
		$reinitLink = "";
		if (empty($params["POST"])) {
			$token = new Token();
			$token->createToken(Token::TOKEN_FORGET_PWD);
			$token->save();
		}
		else {
			$tokens = Token::findByColumns(array(
				"token" => $params["POST"]["token"],
				"nom_token" => Token::TOKEN_FORGET_PWD,
				"id_user" => null
			));
			//Si token trouvé inconnu, redirection vers le formulaire d'où on vient
			if (empty($tokens)) {
				$token = null;
				header('Location:'.$_SERVER['PHP_SELF']);
				exit;
			}
			else {
				$token = $tokens[0];
			}

		}
		// Générer les champs de saisis
		$form = $user->configFormMail($token->getToken());

		$errors = [];
		if(!empty($params["POST"])){
			//Verification des saisies
			
			$errors = Validator::validateForgetPwd($form, $params["POST"], $token);
			//si user trouvé
			if(empty($errors)){

				$mailer = new Mailer();
				$email = "";

				//recuperation des champs saisis (email)
				
				$email = $params["POST"]['email'];
				
				$users = User::findByColumn("email", $email);
				$user = $users[0];

				$token = new Token();
				$token->createToken(Token::TOKEN_REINIT_PWD);
				$token->setIdUser($user->getId());
				$token->save();
				$reinitLink = "http://localhost:8043/user/reinitPwd?token=".$token->getToken();

				$name = $user->getFirstname();
				$to = $user->getEmail();
				$subject = "Réinitialisation de votre mot de passe Eventive";
				$body = include("views/mail/msgreinit.php");
				$altBody = "Bonjour ".$name.", \rn"." tu peux réinitialiser ton mot de passe sur ce lien : ".$reinitLink."\rn"."Cordialement"."\rn"."Eventive";

				if($mailer->sendMail($to, $name, $subject, $body, $altBody)) {
					$v = new View("confirmation", "special");
					exit();
				}
			}
		}
		$v = new View("forgetpwd", "special");
		$v->assign("config",$form);
		$v->assign("old", $params["POST"]);
		$v->assign("errors",$errors);

	}
	//Reinitialisation mdp
	public function reinitPwdAction($params) {

		$token_value = $params['GET']['token'];
		$tokens = Token::findByColumn('token',$token_value);

		if (empty($tokens)) {
			// Affiche une erreur et exit
			$v = new View("error", "special");
			exit;
		}

		$token = $tokens[0];

		$user = new User();
		$user->find($token->getIdUser());

		// Générer les champs de saisis
		$form = $user->configFormReinitPwd();

		$errors = [];

		if(!empty($params["POST"])){
			$errors = Validator::validateReinitPwd($form, $params["POST"], $token);
			
			if(empty($errors)){
				$hash = password_hash($params["POST"]["pwd"], PASSWORD_DEFAULT);
				$user->setPwd($hash);
				$user->save();
				header('Location: /user/connect');
				exit();
			}
		
		}
		$v = new View("reinitpwd", "special");
		$v->assign("config",$form);
		$v->assign("old", $params["POST"]);
		$v->assign("errors",$errors);

	}

	//Inscription
	public function addAction($params){
		$user = new User();
		$captcha = true;
		if (empty($params["POST"])) {
			$token = new Token();
			$token->createToken(Token::TOKEN_ADD_USER);
			$token->save();
		}
		else {
			$tokens = Token::findByColumns(array(
				"token" => $params["POST"]["token"],
				"nom_token" => Token::TOKEN_ADD_USER,
				"id_user" => null
			));
			if (empty($tokens)) {
				$token = null;
				header('Location:'.$_SERVER['PHP_SELF']);
				exit;
			}
			else {
				$token = $tokens[0];
			}
		}

		$form = $user->configFormAdd($token->getToken());
		$errors = [];
		if(!empty($params["POST"])){
			//Verification des saisies
			
			$errors = Validator::validate($form, $params["POST"], $token);

			if(empty($errors)){
				$user->setFirstname($params["POST"]["firstname"]);
				$user->setLastname($params["POST"]["lastname"]);
				$user->setBirthday($params["POST"]["birthday"]);
				$user->setEmail($params["POST"]["email"]);
				$hash = password_hash($params["POST"]["pwd"], PASSWORD_DEFAULT);
				$user->setPwd($hash);
				$user->save();
				$_SESSION['id'] = $user->getId();
			}
		}
		
		$v = new View("addUser", "special");
		$v->assign("config",$form);
		$v->assign("captcha",$captcha);
		$v->assign("old",$params["POST"]);
		$v->assign("errors",$errors);

	}
	
	//Connexion
	public function connectAction($params) {

		if(parent::isConnected()){
			header('Location: /dashboard/dashboard');
			exit();
		}
		
		$user = new User();
		
		if (empty($params["POST"])) {
			$token = new Token();
			$token->createToken(Token::TOKEN_CONNECTION);
			$token->save();
		}
		else {
			$tokens = Token::findByColumns(array(
				"token" => $params["POST"]["token"],
				"nom_token" => Token::TOKEN_CONNECTION,
				"id_user" => null
			));
			if (empty($tokens)) {
				$token = null;
				header('Location:'.$_SERVER['PHP_SELF']);
				exit;
			}
			else {
				$token = $tokens[0];
			}
		}
		// Générer les champs de saisis
		$form = $user->configFormConnect($token->getToken());

		$errors = [];
		if(!empty($params["POST"])){
			//Verification des saisies
			
			$errors = Validator::validateLogin($form, $params["POST"], $token);
			//si user trouvé
			if(empty($errors)){
				header('Location: /dashboard/dashboard');
				exit;
			}
		}
		$v = new View("connect", "special");
		$v->assign("config",$form);
		$v->assign("old", $params["POST"]);
		$v->assign("errors",$errors);

	}

	public function removeAction($params){
		echo "Action de suppression d'un user";
		echo "<pre>";
		sleep(5);
		print_r($params["POST"]["id"]);
	}

	function disconnectAction() {
		if (parent::isConnected()) {
			unset($_SESSION['id']);
			header('Location: /user/connect');
			exit();
		}

	}

}