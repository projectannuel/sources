<pre>
<?php //print_r($config);?>
<?php //print_r($errors);?>
</pre>

<?php 
if(!empty($_POST)){
	$data=$_POST;
	if(!empty($errors)){print_r($errors);}
	else{echo "La modification a été effectuée.";}
} 
	
?>		

<form id="createPage" class="form-light-container" method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>">
		
	<?php foreach ($config["input"] as $name => $params):?>
	
		<?php if($params["type"] == "text"):?>
			<div class="container">
				<?php echo $params["placeholder"];?>
			</div>

			<input 
				class="form-light <?php echo $params["class"];?>"
				type="<?php echo $params["type"];?>" 
				name="<?php echo $name;?>"
				value ="<?php echo (isset($data[$name]))?$data[$name]:"";?>"
				<?php echo (isset($params["required"]))?"required='required'":"";?>
				>
		<?php endif;?>

		<?php if($params["type"] == "file"):?>
			<div class="container">
				<?php echo $params["placeholder"];?>
			</div>

			<input 
				class="form-light"
				type="<?php echo $params["type"];?>" 
				name="<?php echo $name;?>"
				value ="<?php echo (isset($data[$name]))?$data[$name]:"";?>"
				<?php echo (isset($params["required"]))?"required='required'":"";?>
				>
		<?php endif;?>

	<?php endforeach;?>
	<input onClick = "event.preventDefault();tinyMCE.triggerSave();$( '#createPage' ).submit();" class="form-light-button" type="submit" value="<?php echo $config["config"]["submit"];?>">

</form>