
<ul>
	<?php foreach ($errors as $error) {
		echo '<li>'.$error.'</li>';
	} ?>
</ul>

<form method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>">
		
	<?php foreach ($config["input"] as $name => $params):?>
		
		<?php if($params["type"] == "date" || $params["type"] == "text" || $params["type"] == "email" || $params["type"] == "password" || $params["type"] == "hidden"):?>
			<label><?php echo $params["type"]=="hidden" ? "" : $name;?></label>
		
			<input
				value="<?php if (isset($old[$name])) echo $old[$name]; else if (isset($params["value"])) echo $params["value"]; ?>"
				type="<?php echo $params["type"];?>" 
				name="<?php echo $name;?>"
				placeholder="<?php 
				if(!empty($params["placeholder"])){
					echo $params["placeholder"];
				}
				?>"
				<?php echo (isset($params["required"]))?"required='required'":"";?>
			>
		<?php endif;?>
		
	<?php endforeach;?>
	
	<img class="captcha" src='/captcha/captcha'>

	<input class="btn btn-blue" type="submit" value="<?php echo $config["config"]["submit"];?>">
	
</form>