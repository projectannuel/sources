<pre>
<?php //print_r($config);?>
<?php //print_r($errors);?>
</pre>

<?php 
if(!empty($_POST)){
	$data=$_POST;
	if(!empty($errors)){print_r($errors);}
	else{echo "La modification a été effectuée.";}
} 
	
?>		

<form class="form-light-container" method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>">
		
	<?php foreach ($config["input"] as $name => $params):?>
	
		<?php if($params["type"] == "text" || $params["type"] == "email" || $params["type"] == "password"):?>
			<input 
				class="form-light"
				type="<?php echo $params["type"];?>" 
				name="<?php echo $name;?>"
				placeholder="<?php echo $params["placeholder"];?>"
				value ="<?php echo (isset($data[$name]))?$data[$name]:"";?>"
				<?php echo (isset($params["required"]))?"required='required'":"";?>
				>

		<?php endif;?>

		<?php if($params["type"] == "color"):?>
			<div class="container-fluid">
        		<div class="row form-light-line">
					<div class="form-light-color col-md-10">Couleur pour : <?php echo $params["placeholder"];?></div>
				<input 
					class="form-light-color col-md-2"
					type="<?php echo $params["type"];?>" 
					name="<?php echo $name;?>"
					value ="<?php echo (isset($data[$name]))?$data[$name]:"";?>"
					<?php echo (isset($params["required"]))?"required='required'":"";?>
					><br>
				</div>
			</div>
		<?php endif;?>

		<?php if($params["type"] == "radio"):?>
		<div class="form-light-line col-md-12">
			<?php echo $params["placeholder"];?><br>
			<?php foreach ($params["choices"] as $id => $name):?>
				<input
				id="<?php echo 'choix'.$id;?>" 
				type="<?php echo $params["type"];?>" 
				name="choice"
				value ="<?php echo $name;?>"
				<?php echo (isset($params["required"]))?"required='required'":"";?>
				>
				<label for="<?php echo 'choix'.$id;?>"><?php echo $name;?></label>
				
			<?php endforeach;?>	
			</div>

		<?php endif;?>
	<?php endforeach;?>
	<input class="form-light-button" type="submit" value="<?php echo $config["config"]["submit"];?>">

</form>