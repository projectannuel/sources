
<div class="row full-height">
	<div class="divCo col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-4 col-md-4">
	<div id="divLogo" class="col-xs-offset-2 col-xs-8 col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-12">
		<img src="../public/img/logo.png" id="logo" title="logo">
	</div>
		<?php 
			$this->addModal("form", $config, $old, $errors); 
		?>
		<p><span id="texteCo">Mot de passe oublié ?</span> <strong><a class="linkCo" href="/user/forgetPwd" alt="Clique ici pour récupérer ton mot de passe !">Clique ici !</a></strong><p>
		<p><span id="texteCo">Pas encore inscrit ?</span> <strong><a class="linkCo" href="/user/add" alt="Clique ici pour t'inscrire!">Inscrivez-vous !</a></strong><p>
	</div>
</div>
