<?php 
	if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>E-ventive</title>
		<link rel="stylesheet" href="/public/css/style.css" type="text/css">
	</head>
	<body id="connectBody">
		<div id="forgetPwd">
		
			<?php include "views/".$this->v; ?>
		
		</div>
	</body>

</html>