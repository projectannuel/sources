<style>
  .mce-notification-warning{
    display:none;
  }
</style>

<main role="main" class="col-lg-8 col-xl-9">
  <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12">
          <?php $this->addModal("pageForm", $config, $errors, $lastValues); ?>
        </div>
      </div>
    </div>
</main>