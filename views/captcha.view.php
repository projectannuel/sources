<?php
	$imgWidth = 400;
	$imgHeight = 150;
	//Créer une image
	$image = imagecreate($imgWidth, $imgHeight);
	
	$back = imagecolorallocate($image, rand(0,100), rand(0,100), rand(0,100));

	$char = "abcdefghijklmonpqrstuvwxyz0123456789";
	$char = str_shuffle($char);
	$length = rand(-8,-6);
	$captcha = substr($char, $length);

	$fonts = glob("public/fonts/*.ttf");
	$x = rand(20,30);
	for($i=0; $i<strlen($captcha);$i++) {

		$size = rand(20,30);

		$angle = rand(-30,30);

		//40 pourla marge
		$y = rand(50, $imgHeight-50);

		$color = imagecolorallocate($image, rand(150,255), rand(150,255), rand(150,255));

		imagettftext($image, $size, $angle, $x, $y, $color, $fonts[rand(0, count($fonts)-1)], $captcha[$i]);

		$x += $size+rand(20,20);
	}

	for($j=0; $j < rand(4,6); $j++) {
		
		$x1 = rand(0, $imgWidth);
		$x2 = rand(0, $imgWidth);
		$y1 = rand(0, $imgHeight);
		$y2 = rand(0, $imgHeight);
		$color = imagecolorallocate($image, rand(150,255),rand(150,255),rand(150,255));
		
		switch(rand(0,2)) {
			case 0:
				imageline($image, $x1, $y1, $x2, $y2, $color);
				break;
			case 1:
				imagerectangle($image, $x1, $y1, $x2, $y2, $color);
				break;
			default:
				imageellipse($image, $x1, $y1, $x2, $y2, $color);
				break;
		}
	}


	//Afficher l'image (ou l'enregistrer)
	imagepng($image);

	$_SESSION['captcha'] = $captcha;