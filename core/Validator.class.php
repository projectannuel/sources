<?php
//Interdire mail identiques
class Validator{
	//Inscription

	public static function validateA($form, $params){
		$errorsMsg = [];

		foreach ($form["input"] as $name => $config) {
			
			if(isset($config["confirm"]) &&  $params[$name]!==$params[$config["confirm"]] ){
					$errorsMsg[] = $name." doit être identique à ".$config["confirm"];
			}else if(!isset($config["confirm"])){
				if($config["type"]=="email" && !self::checkEmail($params[$name])){
					$errorsMsg[] = "L'email n'est pas valide";
				}
				else if($config["type"]=="password" && !self::checkPwd($params[$name])){
					$errorsMsg[] = "Le mot de passe est incorrect (6 à 12, min, maj, chiffres)";
				}
			}

			if($config["type"]=="color" && !self::checkColor($params[$name])){
				$errorsMsg[] = "La valeur de ".$name." doit-être tapée comme ceci : #000000";
			}

			if(isset($config["required"]) && !self::minLength($params[$name], 1)){
					$errorsMsg[] = $name." doit faire plus de 1 caractère";
			}

			if(isset($config["minString"]) && !self::minLength($params[$name], $config["minString"])){
					$errorsMsg[] = $name." doit faire plus de ".$config["minString"]." caractères";
			}

			if(isset($config["maxString"]) && !self::maxLength($params[$name], $config["maxString"])){
					$errorsMsg[] = $name." doit faire moins de ".$config["maxString"]." caractères";
			}

		}

		return $errorsMsg;
	}

	public static function validate($form, $params, $token){
		$errorsMsg = [];

		foreach ($form["input"] as $name => $config) {
			if(isset($config["confirm"]) &&  $params[$name]!==$params[$config["confirm"]] ){
				$errorsMsg[] = $name." doit être identique à ".$config["confirm"];
			}else if(!isset($config["confirm"])){
				if($config["type"]=="email" && self::checkEmail($params[$name])){
					$errorsMsg[] = "Cet adresse e-mail est déjà utilisé.";
				} else if($config["type"]=="password" && !self::checkPwd($params[$name])){
					$errorsMsg[] = "Le mot de passe est incorrect (6 à 12, min, maj, chiffres)";
				}
			}
			if(isset($config["required"]) && !self::minLength($params[$name], 1)){
				if($name!= "token")
					$errorsMsg[] = $name." doit faire plus de 1 caractère";
			}
			if(isset($config["minString"]) && !self::minLength($params[$name], $config["minString"])){
				$errorsMsg[] = $name." doit faire plus de ".$config["minString"]." caractères";
			}
			if(isset($config["maxString"]) && !self::maxLength($params[$name], $config["maxString"])){
				$errorsMsg[] = $name." doit faire moins de ".$config["maxString"]." caractères";
			}
		}
		if(isset($_SESSION["captcha"])) {
			if(!isset($params["captcha"]) || strtolower($params["captcha"]) != $_SESSION["captcha"]) {
				$errorsMsg[] = "Captcha incorrect !";
			}
		}

		if ($token == null || !$token->isValid()) {
			$errorsMsg[] = "Le jeton est invalide.";
		}
		else {
			if (empty($errorsMsg)) {
				
				$token->remove();
			}
		}
		
		// Supprime tous les tokens expires
		$tokens = Token::findByCondition("date_expiration", time(), "<");
		foreach($tokens as $token) {
			$token->remove();
		} 

		return $errorsMsg;
	}
	

	//Connexion
	public static function validateLogin($form, $params, $token){

		$errorsMsg = [];
		$email = "";
		$pwd = "";

		//recuperation des champs saisis (email et pwd)
		foreach ($form["input"] as $name => $config) {
			if(isset($config["required"])) {
				if($config["type"]=="email" && self::checkEmail($params[$name])){
					$email = $params[$name];
				} else if($config["type"]=="password"){
					$pwd = $params[$name];
				}
			}
		}

		//verification de la concordance de l'email avec pwd
		$user = self::verifyPwd($email,$pwd);

		if(!$user) {
			$errorsMsg[] = "Les saisies sont incorrectes";
		}

		/*---------------
		--gestion token--
		-----------------*/
		if ($token == null || !$token->isValid()) {
			$errorsMsg[] = "Le jeton est invalide.";
		}
		else {
			if (empty($errorsMsg)) {
				$_SESSION['id'] = $user->getId();
				$token->remove();
			}
		}

		// Supprime tous les tokens expires
		$tokens = Token::findByCondition("date_expiration", time(), "<");
		foreach($tokens as $token) {
			$token->remove();
		}
		return $errorsMsg;
	}

	//Mdp oublie
	public static function validateForgetPwd($form, $params, $token){

		$errorsMsg = [];
		$email = "";

		//recuperation des champs saisis (email et pwd)
		foreach ($form["input"] as $name => $config) {
			if(isset($config["required"])) {
				if($config["type"]=="email" && self::checkEmail($params[$name])){
					$email = $params[$name];
				}
			}
		}

		//verification de la concordance de l'email avec pwd
		$user = self::verifyMail($email);

		if(!$user) {
			$errorsMsg[] = "Les saisies sont incorrectes";
		}

		/*---------------
		--gestion token--
		-----------------*/

		if ($token == null || !$token->isValid()) {
			$errorsMsg[] = "Le jeton est invalide.";
		}
		else {
			if (empty($errorsMsg)) {
				$_SESSION['id'] = $user->getId();
				$token->remove();
			}
		}

		// Supprime tous les tokens expires
		$tokens = Token::findByCondition("date_expiration", time(), "<");
		foreach($tokens as $token) {
			$token->remove();
		}
		return $errorsMsg;
	}

	//Reinitialisation de mdp
	public static function validateReinitPwd($form, $params, $token){
		$errorsMsg = [];

		foreach ($form["input"] as $name => $config) {
			if(isset($config["confirm"]) &&  $params[$name]!==$params[$config["confirm"]] ){
				$errorsMsg[] = $name." doit être identique à ".$config["confirm"];
			}else if(!isset($config["confirm"])){
				if($config["type"]=="password" && !self::checkPwd($params[$name])){
					$errorsMsg[] = "Le mot de passe est incorrect (6 à 12, min, maj, chiffres)";
				}
			}
			if(isset($config["required"]) && !self::minLength($params[$name], 1)){
				if($name!= "token")
					$errorsMsg[] = $name." doit faire plus de 1 caractère";
			}
		}

		if ($token == null || !$token->isValid()) {
			$errorsMsg[] = "Le jeton est invalide.";
		}
		else {
			if (empty($errorsMsg)) {
				
				$token->remove();
			}
		}
		
		// Supprime tous les tokens expires
		$tokens = Token::findByCondition("date_expiration", time(), "<");
		foreach($tokens as $token) {
			$token->remove();
		} 

		return $errorsMsg;
	}
	/*
		Les fonctions de verifications
	*/

	public static function checkEmail($email){
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$users = User::findByColumn("email", $email);
			return !empty($users);
		}
		return false;
	}
	public static function verifyMail($email){
		$users = User::findByColumn("email", $email);
		if(!empty($users)) {
			$user = $users[0];
			if (!empty($user)) {
				return $user;
			}
			return NULL;
		}
		return NULL;
	}
	public static function verifyPwd($email, $pwd){
		$users = User::findByColumn("email", $email);
		$connecte = false;
		if(!empty($users)) {
			$user = $users[0];
			$connecte = password_verify($pwd, $user->getPwd());
			if ($connecte) {
				return $user;
			}
			return NULL;
		}
		return NULL;
	}
	
	public static function checkPwd($pwd){
		return self::minLength($pwd, 6)
			&& self::maxLength($pwd, 12)
			&& preg_match("/[A-Z]/", $pwd)
			&& preg_match("/[a-z]/", $pwd)
			&& preg_match("/[0-9]/", $pwd);
	}

	public static function minLength($value, $length){
		return strlen(trim($value))>=$length;
	}
	public static function maxLength($value, $length){
		return strlen(trim($value))<=$length;
	}

	public static function checkColor($color){
		return preg_match("/#([a-zA-Z0-9]{6})/", $color);
	}

}



