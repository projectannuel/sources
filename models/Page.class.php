<?php 
class Page extends BaseSql{

	protected $id = null;
	protected $userId = null;
	protected $name;
	protected $templateId;
	protected $typePageId;

	public function __construct(){parent::__construct();}

	public function getId(){return $this->id;}
	public function setId($id){$this->id = $id;}

	public function getUserId(){return $this->userId;}
	public function setUserId($userId){$this->userId = $userId;}

	public function getName(){return $this->name;}
	public function setName($name){$this->name = $name;}

	public function getTemplateId(){return $this->templateId;}
	public function setTemplateId($templateId){$this->templateId = $templateId;}

	public function getTypePageIdt(){return $this->typePageId;}
	public function setTypePageId($typePageId){$this->typePageId = $typePageId;}

	public function PageFormEdit(){

		$values = [];	
		$allTypes = $typeElement->getAll();

		foreach($allTypes as $type){
			$values[$type] = ["type"=>$type['type'],"required"=>true];
		}

		return [
					"config"=>["method"=>"POST", "action"=>"", "submit"=>"Modifier cet élément"],
					"input"=> $values
				];
	}

	public function getIdByName($name){
		$val = $this->findIdByName($name);
		$variable = [];
		foreach ($val as $row) {$variable[$row['id']] = [$row['name'],$row['type']];}
		return $variable;
	}
}