<?php 
class TypeElement extends BaseSql{

	protected $id = null;
	protected $name = null;

	public function __construct(){parent::__construct();}

	public function getId(){return $this->id;}
	public function setId($id){$this->id = $id;}

	public function getName(){return $this->name;}
	public function setName($name){$this->name = $name;}

	public function getAll(){
		$val = $this->findAll();
		$variable = [];
		foreach ($val as $row) {$variable[$row['id']] = [$row['name'],$row['type']];}
		return $variable;
	}
}
