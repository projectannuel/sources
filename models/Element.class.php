<?php 
class Element extends BaseSql{

	protected $id = null;
	protected $typeelement = null;
	protected $pagesId;
	protected $value;

	public function __construct(){parent::__construct();}

	public function getId(){return $this->id;}
	public function setId($id){$this->id = $id;}

	public function getTypeelement(){return $this->typeelement;}
	public function setTypeelement($typeelement){$this->typeelement = $typeelement;}

	public function getPagesId(){return $this->pagesId;}
	public function setPagesId($pagesId){$this->pagesId = $pagesId;}

	public function getValue(){return $this->value;}
	public function setValue($id){$this->value = $value;}

	
	public function ElementFormEdit(){

		$typeElement = new TypeElement();

		$values = [];	
		$allTypes = $typeElement->getAll();

		foreach($allTypes as $type){
			$values[$type[0]] = ["type"=>$type[1],"placeholder"=>"Définissez : $type[0]","required"=>true,"class"=>"tinymce"];
		}

		return [
					"config"=>["method"=>"POST", "action"=>"#","submit"=>"Modifier cet élément"],
					"input"=> $values
				];
	}

	public function getAll(){
		$val = $this->findAll();
		$variable = [];
		foreach ($result as $row) {$variable[$row['id']] = $row['name'];}
		return $variable;
	}

	public function getById($id){
		return $this->findById($id);
	}

}
