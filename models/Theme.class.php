<?php 
class Theme extends BaseSql{

	protected $id = null;
	protected $font1;
	protected $font2;
	protected $fontcolor;
	protected $color1;
	protected $color2;
	protected $color3;
	protected $color4;
	protected $colorbackground;
	protected $logo;
	protected $choice;

	public function __construct(){parent::__construct();}

	public function getId(){return $this->id;}
	public function setId($id){$this->id = $id;}

	public function getFont1(){return $this->font1;}
	public function setFont1($font1){$this->font1 = $font1;}

	public function getFont2(){return $this->font2;}
	public function setFont2($font2){$this->font2 = $font2;}
 
	public function getFontColor(){return $this->fontcolor;}
	public function setFontColor($fontcolor){$this->fontcolor = $fontcolor;}

	public function getColor1(){return $this->color1;}
	public function setColor1($color1){$this->color1 = $color1;}

	public function getColor2(){return $this->color2;}
	public function setColor2($color2){$this->color2 = $color2;}
 
	public function getColor3(){return $this->color3;}
	public function setColor3($color3){$this->color3 = $color3;}

	public function getColor4(){return $this->color4;}
	public function setColor4($color4){$this->color4 = $color4;}

	public function getColorBackground(){return $this->colorbackground;}
	public function setColorBackground($colorbackground){$this->colorbackground = $colorbackground;}

	public function getLogo(){return $this->logo;}
	public function setLogo($logo){$this->logo = $logo;}

	public function getChoice(){return $this->choice;}
	public function setChoice($choice){$this->choice = $choice;}

	public function configFormEdit(){

		$template = new Template();

		return [
					"config"=>["method"=>"POST", "action"=>"", "submit"=>"Modifier le Thème"],
					"input"=>[
						
						"font1"=>["type"=>"text","placeholder"=>"Nom de la Police : Titres","required"=>true,"maxString"=>100],
						"font2"=>["type"=>"text","placeholder"=>"Nom de la Police : Contenus","required"=>true, "minString"=>2, "maxString"=>100],
						"fontcolor"=>["type"=>"color","placeholder"=>"Couleur du texte","required"=>true],
						"color1"=>["type"=>"color","placeholder"=>"Couleur numéro 1","required"=>true],
						"color2"=>["type"=>"color","placeholder"=>"Couleur numéro 2","required"=>true],
						"color3"=>["type"=>"color","placeholder"=>"Couleur numéro 3","required"=>true],
						"color4"=>["type"=>"color","placeholder"=>"Couleur numéro 4","required"=>true],
						"colorbackground"=>["type"=>"color","placeholder"=>"Couleur du Papier Peint","required"=>true],
						"logo"=>["type"=>"text","placeholder"=>"Url du Logo","required"=>true],
						"choice"=>["type"=>"radio","placeholder"=>"Choix du Template","choices"=>$template->getNames(),"required"=>true],
					]
				];
	}
}