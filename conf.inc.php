<?php

define("DBUSER","root");
define("DBPWD","password");
define("DBHOST","database");
define("DBNAME","eventive");
define("DBPORT","3306");
define("DBDRIVER","mysql");

define("DS", "/");

$scriptName = (dirname($_SERVER["SCRIPT_NAME"]) == "/")?"":dirname($_SERVER["SCRIPT_NAME"]);
define("DIRNAME", $scriptName.DS);
